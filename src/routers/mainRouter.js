import React from 'react';
import {
    createAppContainer,
    createSwitchNavigator,
    createBottomTabNavigator,
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SplashScreen from "../screens/auth/SplashScreen";
import Landing from "../screens/auth/Landing";
import Login from "../screens/auth/Login"
import Dashboard from "../screens/dashboard/Dashboard";
import MenuDrinks from "../screens/dashboard/MenuDrinks";
import MenuFood from "../screens/dashboard/MenuFood";
import MenuMerchandise from "../screens/dashboard/MenuMerchandise";

export const All = createStackNavigator({
    SplashScreen: {
        screen: SplashScreen
    },
    Landing: {
        screen: Landing
    },
    Login:{
        screen: Login
    },
    Dashboard: {
        screen: Dashboard
    },
    MenuDrinks:{
        screen: MenuDrinks
    },
    MenuFood:{
        screen: MenuFood
    },
    MenuMerchandise:{
        screen: MenuMerchandise
    }
});

export const Root = createAppContainer(All)

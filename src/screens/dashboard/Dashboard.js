import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    ActivityIndicator,
    AsyncStorage,
    FlatList,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    Alert,
    Platform,
    TextInput,
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Card,
    CardItem,
    Textarea
} from "native-base";
import SubApproval from "../../component/card/SubApproval";
import Loader from "../../component/loader/loader";
import colors from "../../component/styles/colors/index";
import {RFValue} from "react-native-responsive-fontsize";
import size from "../../component/styles/size/index";
import SwipeUpDown from 'react-native-swipe-up-down';
import GlobalConfig from "../../component/server/GlobalConfig";
import CustomHeader from "../../component/header/CustomHeader";
const bg_assets = require("../../component/assets/Background_Home.jpg");
const qr = require("../../component/assets/qr.png");
import SwipeablePanel from 'rn-swipeable-panel';

class ListMini extends React.PureComponent {
    render() {
        return (
            <View>

            </View>
        )
    }
}

class ListFull extends React.PureComponent {
    render() {
        return (
            <View>
                <Image style={styles.imageMenu} source={that.props.loading == true ? loading : {uri : this.props.data.gambar}}/>
            </View>

        )
    }
}
export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            swipeablePanelActive: false,
            dataUser :
                {
                    "salam": "How's your day? Need a coffee break?",
                    "beans": 1,
                    "point": 1,
                    "balance": 279000,
                    "username": "Guntur Saputro",
                    "primaryCard": {
                        "id_card": "777",
                        "card_number": "6976988222777077",
                        "card_name": "card01",

                        "card_image": "http://maxxassets.xyz/image/cards/card-default-8.png",

                        "barcode":
                            "http://maxxassets.xyz/image/barcode/sjabp69fvc.jpg",
                        "distribution_id": "89972081901640006976988222777077",
                        "card_pin": "191292",
                        "beans": 1,
                        "expired_date": "2017-02-10 23:59:59",
                        "balance": 279000
                    },
                    "virtual_card": 0,
                    "email": "support@technopartner.id",
                    "phone": "083847090000",
                    "referralCode": "U0003312",
                    "verifikasi_email": "yes",
                    "verifikasi_sms": "yes",
                    "cardAmount": 1
                }

        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
        // AsyncStorage.getItem("accessToken").then(accessToken => {
        //     AsyncStorage.getItem("dataUser").then(dataUser => {
        //         this.setState({
        //             dataUser: dataUser,
        //             access_token: JSON.parse(accessToken).access_token,
        //             token_type: JSON.parse(accessToken).token_type,
        //         })
        //         // this.loadData()
        //     })
        // })
        this.openPanel();
    }

    Alert(message){
        Alert.alert(
            'Information',
            message,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false }
        );
    }

    loadData(){
        // this.setState({
        //     loading:true
        // })
        var url = GlobalConfig.URL_SERVER + 'api/login';
        fetch(url, {
            headers: {
                'Authorization' :  this.state.token_type + ' '+ this.state.access_token,
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            method: 'POST',
            body: JSON.stringify({
                token : JSON.parse(this.state.dataUser).token,
            })
        }).then((response) => response.json())
            .then((response) => {

                alert(JSON.stringify(response))
                // if(response.status == 'success'){
                //     this.setState({
                //         loading:false
                //     })
                //     AsyncStorage.setItem('dataUser', JSON.stringify(response)).then(() => {
                //         this.props.navigation.navigate('Dashboard')
                //     })
                // }
            })
            .catch((error) => {
                this.setState({
                    loading:false
                })
                setTimeout(() =>
                        this.Alert('Cek Koneksi Internet Anda')
                    , 312);
            })
    }

    openPanel = () => {
        this.setState({ swipeablePanelActive: true });
    };

    closePanel = () => {
        this.setState({ swipeablePanelActive: false });
    };

    load(){
        alert(JSON.stringify(this.state.dataUser))
    }

    // _renderMini = ({ item }) => <ListMini data={item}/>
    // _renderFull = ({ item }) => <ListFull data={item}/>

    render() {
        return (
            <View>
                <Loader loading={this.state.loading} />
                <ImageBackground source={bg_assets} style={{ width: '100%', height: '100%' }}>
                    <CustomHeader transparent navigation={this.props.navigation} title="" left={true} right={false}/>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={styles.content}>
                            <View style={{width: '100%', height: '10%'}}>
                                <Text style={styles.titleGood}>{this.state.dataUser.salam}</Text>
                                <Text style={styles.titleGood}>{this.state.dataUser.username}</Text>
                                <Text style={styles.titleGood}>IDR {this.state.dataUser.balance}</Text>
                                <Text style={styles.titleGood}>{this.state.dataUser.beans} Beans</Text>
                            </View>
                            <View style={{height: '40%'}}/>
                            <SwipeablePanel
                                noBackgroundOpacity={true}
                                fullWidth
                                isActive={this.state.swipeablePanelActive}
                                onClose={this.closePanel}
                                onPressCloseButton={this.closePanel}
                                style={{backgroundColor: colors.white_1st, opacity: 0.8,}}
                            >
                                <View style={{padding:RFValue(20)}}>
                                    <Text style={styles.prime}>Prime to Pay</Text>
                                    <View style={{borderBottomWidth:1, borderColor:colors.gray_1st}}></View>
                                    <Text style={styles.show}>Show below QR Code to the cashier</Text>
                                    <Text style={styles.cardName}>{this.state.dataUser.primaryCard.card_name}</Text>
                                    <View style={{flex:1, flexDirection:'row'}}>
                                        <View style={{width:'60%'}}>
                                            <Text style={styles.cardNameGreen}>Balance</Text>
                                            <Text style={styles.cardNameGreen}>Beans</Text>
                                        </View>
                                        <View style={{width:'30%', alignItems: 'flex-end'}}>
                                            <Text style={styles.cardName}>IDR {this.state.dataUser.primaryCard.balance}</Text>
                                            <Text style={styles.cardName}>{this.state.dataUser.primaryCard.beans}</Text>
                                        </View>
                                    </View>
                                    <View style={{justifyContent:'center', marginTop:RFValue(20)}}>
                                        <Image style={styles.qr} source={qr}/>
                                        <Button
                                            block
                                            style={{
                                                width:'100%',
                                                height: RFValue(35),
                                                marginBottom: 0,
                                                backgroundColor: colors.green_1st,
                                                borderRadius: 5,
                                                marginTop:RFValue(10)
                                            }}
                                            onPress={() => this.props.navigation.navigate('MenuDrinks')}
                                        >
                                            <Text style={styles.btnSign}>VIEW MENU</Text>
                                        </Button>
                                    </View>
                                </View>
                            </SwipeablePanel>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    prime:{
        fontSize: RFValue(20),
        color: colors.black_1st,
        marginBottom: RFValue(10),
    },
    qr:{
        width:RFValue(300),
        height:RFValue(300)
    },
    show:{
        fontSize: RFValue(15),
        color: colors.gray_1st,
        marginBottom: RFValue(5),
        marginTop: RFValue(10)
    },
    cardName:{
        fontSize: RFValue(13),
        color: colors.black_1st,
    },
    cardNameGreen:{
        fontSize: RFValue(13),
        color: colors.green_1st,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    content: {
        flex: 1, flexDirection: 'column',
        paddingHorizontal: RFValue(10),
    },
    titleGood: {
        fontSize: RFValue(15),
        color: colors.white_1st,
    },
    iconNav: {
        width: RFValue(20),
        height: RFValue(20),
        resizeMode: "stretch",
    },
    titleLogin: {
        fontSize: RFValue(30),
        color: colors.blue_2st,
        fontFamily: 'Montserrat-Regular',
    },
    signIn: {
        fontSize: size.small_04,
        color: colors.gray_1st,
        marginLeft: RFValue(10),
        fontFamily: 'Montserrat-Regular',
        fontWeight: 'bold',
    },
    forgot: {
        fontSize: size.small_03,
        color: colors.gray_1st,
        textAlign: 'right'
    },
    dont: {
        color: colors.gray_1st,
        fontSize: size.small_03,
        textAlign: 'center'
    },
    create: {
        color: colors.black_1st,
        fontSize: size.small_03,
        fontWeight: 'bold'
    },
    btnSign: {
        color: colors.white_1st,
        fontSize: RFValue(12),
        fontFamily: 'Montserrat-Regular',
    },
    icon: {
        justifyContent: 'center',
        marginLeft: RFValue(10),
        width: "10%",
    },
    loginButton: {
        marginBottom: 20,
    },
    flex: {
        flex: 1,
        flexDirection: 'row'
    },
    card: {
        marginLeft: RFValue(40),
        marginRight: RFValue(40),
        borderRadius: 50,
        marginTop: RFValue(18),
        backgroundColor: colors.white_1st,
        height: RFValue(35),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 5,
    },
    rowField: {
        flex: 1,
        marginLeft: 0,
        justifyContent: 'center'
    },
    rowFieldPassword: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        marginLeft: 0,
    },
    input: {
        fontSize: RFValue(14),
        color: colors.gray,
        marginLeft: RFValue(5),
        marginRight: 15,
        fontFamily: 'Montserrat-Regular',
    },
    inputPassword: {
        fontSize: RFValue(14),
        color: colors.gray,
        marginLeft: RFValue(5),
        marginRight: 15,
        width: '80%',
        fontFamily: 'Montserrat-Regular',
    }
});

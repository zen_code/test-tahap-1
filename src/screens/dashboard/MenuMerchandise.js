import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    AsyncStorage,
    TouchableOpacity,
    Alert,
    TextInput,
    ImageBackground,
    ScrollView,
    FlatList
} from "react-native";
import {
    Button,
    Footer,
    FooterTab,
    Container, Textarea,
    Thumbnail, List, ListItem, Separator
} from "native-base";
import colors from "../../component/styles/colors/index";
import {RFValue} from "react-native-responsive-fontsize";
import size from "../../component/styles/size/index";
import IconMI from "react-native-vector-icons/MaterialIcons";
import Loader from "../../component/loader/loader";
import CustomHeader2 from "../../component/header/CustomHeader2";
import CustomFooter from "../../component/footer/CustomFooter";
import GlobalConfig from "../../component/server/GlobalConfig";
const nav = require("../../component/assets/Arrow_Menu.png");
const empty = require("../../component/assets/empty_data.png");
const loading = require("../../component/assets/Loading.gif");
import {Collapse,CollapseHeader, CollapseBody, AccordionList} from 'accordion-collapse-react-native';
import MyView from "../../component/view/MyView";

var that;

export default class MenuFood extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            listData:[]
        };
    }

    static navigationOptions = {
        header: null
    };


    render() {
        that=this;
        return (
            <Container>
                <Loader loading={this.state.loading} />
                <CustomHeader2 transparent navigation={this.props.navigation} title="Menu" left={true} right={false}/>
                <CustomFooter navigation={this.props.navigation} tab="MenuMerchandise" />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{padding:RFValue(10),justifyContent:'center', alignItems:'center', marginTop:RFValue(200)}}>
                        <Image
                            style={{ width: RFValue(60), height: RFValue(60)}}
                            source={empty}
                        />
                        <Text style={styles.noData}>Menu not found</Text>
                    </View>
                </ScrollView>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    content:{
        flex: 1, flexDirection: 'column',
        paddingHorizontal:RFValue(10),
    },
    title:{
        fontSize:RFValue(11),
        color:colors.black_1st,
    },
    iconNav: {
        width:RFValue(20),
        height:RFValue(20),
        resizeMode:"stretch",
    },
    headerColl:{
        height:RFValue(40),
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal: RFValue(10),
        backgroundColor: '#e5e5e5'
    },
    titleColl: {
        fontSize:RFValue(13),
        color:colors.black_1st,
        width:'95%'
    },
    signIn: {
        fontSize: size.small_04,
        color:colors.gray_1st,
        marginLeft:RFValue(10),
        fontFamily: 'Montserrat-Regular',
        fontWeight:'bold',
    },
    forgot:{
        fontSize:size.small_03,
        color:colors.gray_1st,
        textAlign:'right'
    },
    dont:{
        color:colors.gray_1st,
        fontSize:size.small_03,
        textAlign:'center'
    },
    create:{
        color:colors.black_1st,
        fontSize:size.small_03,
        fontWeight:'bold'
    },
    btnSign:{
        color:colors.white_1st,
        fontSize:RFValue(12),
        fontFamily: 'Montserrat-Regular',
    },
    icon:{
        justifyContent: 'center',
        marginLeft: RFValue(10),
        width:"10%",
    },
    loginButton: {
        marginBottom: 20,
    },
    flex:{
        flex:1,
        flexDirection:'row'
    },
    noData:{
        fontSize:RFValue(12),
        color:colors.gray_1st,
        marginTop: RFValue(5)
    },
    card:{
        flex:1,
        flexDirection:'column',
        alignItems:'center',
        width:'100%',
        height: 220,
        margin:RFValue(4),
        backgroundColor: '#e5e5e5',
    },
    titleMenu:{
        fontSize:RFValue(12),
        color:colors.black_1st,
        marginTop: RFValue(10),
        textAlign:'center'
    },
    imageMenu:{
        width:'100%',
        height:180,
        resizeMode: 'contain'
    }
});

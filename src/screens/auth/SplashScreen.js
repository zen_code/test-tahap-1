import React from 'react';
import {
    Animated,
    StyleSheet,
    Text
} from 'react-native';
import {
    Container,
} from 'native-base'

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import colors from "../../component/styles/colors/index";
import {RFValue} from "react-native-responsive-fontsize";
import size from "../../component/styles/size/index";

class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
        this.animatedImage = new Animated.Value(0.2);
    }

    static navigationOptions = {
        header: null
    };

    performTimeConsumingTask = async() => {
        return new Promise((resolve) =>
            setTimeout(
                () => { resolve('result') },
                1000
            )
        )
    }

    async componentDidMount() {
        Animated.spring(this.animatedImage, {
            toValue: 1,
            friction: 4,
            delay: 100,
            duration: 100,
            useNativeDriver: true,
        },).start();

        const data = await this.performTimeConsumingTask();

        if (data !== null) {
            this.props.navigation.navigate('Landing');
        }
    }

    render() {
        const imageStyle = {
            transform: [{ scale: this.animatedImage }]
        };

        return (
            <Container style={styles.wrapper}>
                <Animated.View style={[styles.ring, imageStyle]}>
                    <Animated.Image
                        source={require('../../component/assets/Loading.gif')}
                        style={[
                            {
                                resizeMode: "contain",
                                width: wp('50%'),
                                width: hp('10%'),
                            }
                        ]}
                    />
                </Animated.View>
            </Container>
        );
    }
}

export default SplashScreen;


const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        display: "flex",
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white_1st
    },
    ring:{
        alignItems: 'center',
        justifyContent: 'center',
    },
});

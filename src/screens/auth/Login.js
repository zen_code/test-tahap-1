import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    AsyncStorage,
    TouchableOpacity,
    Alert,
    TextInput,
    ImageBackground
} from "react-native";
import {
    Button,
} from "native-base";
import colors from "../../component/styles/colors/index";
import {RFValue} from "react-native-responsive-fontsize";
import size from "../../component/styles/size/index";
import IconMI from "react-native-vector-icons/MaterialIcons";
import Loader from "../../component/loader/loader";
import CustomHeader from "../../component/header/CustomHeader";
import GlobalConfig from "../../component/server/GlobalConfig";
const bg_assets = require("../../component/assets/Background_Login.jpg");
const nav = require("../../component/assets/Nav_Drawer_White.png");

export default class Landing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading : false,
            email:'support@technopartner.id',
            password:'1234567',
            visPass: true,
            hasil:''
        };
    }

    static navigationOptions = {
        header: null
    };

    Alert(message){
        Alert.alert(
            'Information',
            message,
            [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false }
        );
    }

    confirmLogin(){
        let a = 'Input Email Address'
        let b = 'Input Password'
        if(this.state.email == ''){
            this.Alert(a)
        } else if(this.state.password == ''){
            this.Alert(b)
        } else {
            this.generateToken()
        }
    }

    generateToken(){
        var url = GlobalConfig.URL_SERVER + 'oauth/access_token';
        var formData = new FormData();
        formData.append("client_secret", '0a40f69db4e5fd2f4ac65a090f31b823')
        formData.append("client_id", 'e78869f77986684a')
        formData.append("grant_type", 'password')
        formData.append("username", this.state.email)
        formData.append("password", this.state.password)

        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                this.setState({
                    loading:false,
                    access_token: response.access_token,
                    token_type : response.token_type
                })
                AsyncStorage.setItem('accessToken', JSON.stringify(response)).then(() => {

                })
                this.login()
            })
            .catch((error) => {
                this.setState({
                    loading:false
                })
                setTimeout(() =>
                        this.Alert('Cek Koneksi Internet Anda')
                    , 312);
            })
    }

    login(){
        this.setState({
            loading:true
        })
        var url = GlobalConfig.URL_SERVER + 'api/login';
        fetch(url, {
            headers: {
                'Authorization' :  this.state.token_type + ' '+ this.state.access_token,
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            method: 'POST',
            body: JSON.stringify({
                email : this.state.email,
                password : this.state.password
            })
        }).then((response) => response.json())
            .then((response) => {
                if(response.status == 'success'){
                    this.setState({
                        loading:false
                    })
                    AsyncStorage.setItem('dataUser', JSON.stringify(response)).then(() => {
                        this.props.navigation.navigate('Dashboard')
                    })
                }
            })
            .catch((error) => {
                this.setState({
                    loading:false
                })
                setTimeout(() =>
                        this.Alert('Cek Koneksi Internet Anda')
                    , 312);
            })
    }

    render() {
        return (
            <View>
                <Loader loading={this.state.loading} />
                <ImageBackground source={bg_assets} style={{ width: '100%', height: '100%' }}>
                    <CustomHeader transparent navigation={this.props.navigation} title="Login" left={true} right={false}/>
                        <View style={styles.content}>
                            <Text style={styles.title}>Email Address</Text>
                            <View style={styles.rowField}>
                                <TextInput
                                    style={styles.input}
                                    placeholder=''
                                    onChangeText={(text) => this.setState({email: text})}
                                    value={this.state.email}
                                />
                            </View>
                            <Text style={styles.title}>Password</Text>
                            <View style={styles.rowField}>
                                <TextInput
                                    style={styles.input}
                                    placeholder=''
                                    onChangeText={(text) => this.setState({password: text})}
                                    value={this.state.password}
                                />
                            </View>
                            <View style={{height:RFValue(35)}}>
                                <Button
                                    block
                                    style={{
                                        width:'100%',
                                        height: RFValue(35),
                                        marginBottom: 0,
                                        borderWidth: 0,
                                        backgroundColor: colors.green_1st,
                                        borderRadius: 5,
                                    }}
                                    onPress={() => this.confirmLogin()}
                                >
                                    <Text style={styles.btnSign}>LOGIN</Text>
                                </Button>
                            </View>
                            <View>
                                <Text style={styles.not}>Not registered yet? Sign up here</Text>
                                <Text style={styles.forgot}>Forgot Password</Text>
                            </View>

                        </View>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    content:{
        flex: 1,
        flexDirection: 'column',
        padding:RFValue(10),
    },
    title:{
        fontSize:RFValue(12),
        color:colors.white_1st,
        fontFamily: 'Montserrat-Regular',
        marginBottom: RFValue(10)
    },
    not:{
        marginTop:RFValue(10),
        fontSize:RFValue(12),
        color:colors.white_1st,
        textAlign:'center'
    },
    forgot:{
        marginTop:RFValue(10),
        fontSize:RFValue(12),
        color:colors.white_1st,
        textAlign:'center'
    },
    signIn: {
        fontSize: size.small_04,
        color:colors.gray_1st,
        marginLeft:RFValue(10),
        fontFamily: 'Montserrat-Regular',
        fontWeight:'bold',
    },

    dont:{
        color:colors.gray_1st,
        fontSize:size.small_03,
        textAlign:'center'
    },
    create:{
        color:colors.black_1st,
        fontSize:size.small_03,
        fontWeight:'bold'
    },
    btnSign:{
        color:colors.white_1st,
        fontSize:RFValue(12),
        fontFamily: 'Montserrat-Bold',
    },
    icon:{
        justifyContent: 'center',
        marginLeft: RFValue(10),
        width:"10%",
    },
    loginButton: {
        marginBottom: 20,
    },
    flex:{
        flex:1,
        flexDirection:'row'
    },
    card:{
        marginLeft: RFValue(40),
        marginRight: RFValue(40),
        borderRadius: 50,
        marginTop:RFValue(18),
        backgroundColor:colors.white_1st,
        height:RFValue(35),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 5,
    },
    rowField: {
        marginLeft: 0,
        height:RFValue(38),
        backgroundColor: colors.white_1st,
        justifyContent:'center',
        marginBottom:RFValue(15),
        opacity: 0.2,
    },
    rowFieldPassword: {
        flex: 1,
        flexDirection:'row',
        width:'100%',
        marginLeft: 0,
    },
    input: {
        fontSize:RFValue(14),
        color:colors.gray,
        marginLeft:RFValue(5),
        marginRight: 15,
        fontFamily: 'Montserrat-Regular',
    },
    inputPassword: {
        fontSize:RFValue(14),
        color:colors.gray,
        marginLeft:RFValue(5),
        marginRight: 15,
        width:'80%',
        fontFamily: 'Montserrat-Regular',
    }
});

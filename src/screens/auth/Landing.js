import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    AsyncStorage,
    TouchableOpacity,
    Alert,
    TextInput,
    ImageBackground
} from "react-native";
import {
    Button,
} from "native-base";
import colors from "../../component/styles/colors/index";
import {RFValue} from "react-native-responsive-fontsize";
import size from "../../component/styles/size/index";
import IconMI from "react-native-vector-icons/MaterialIcons";
import Loader from "../../component/loader/loader";
import CustomHeader from "../../component/header/CustomHeader";
const bg_assets = require("../../component/assets/Background_Home.jpg");

export default class Landing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading : false,
            username:'',
            password:'',
            visPass: true,
            hasil:''
        };
    }

    static navigationOptions = {
        header: null
    };

    render() {
        return (
            <View>
                <Loader loading={this.state.loading} />
                <ImageBackground source={bg_assets} style={{ width: '100%', height: '100%' }}>
                    <CustomHeader transparent navigation={this.props.navigation} title="" left={true} right={false}/>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={styles.content}>
                            <View style={{width: '100%', height: '10%'}}>
                                <Text style={styles.titleGood}>Good Morning</Text>
                            </View>
                            <View style={{height: '40%'}}/>
                            <View style={{width: '100%', height: '45%', justifyContent: 'flex-end'}}>
                                <View style={{height:RFValue(35)}}>
                                        <Button
                                            block
                                            style={{
                                                width:'100%',
                                                height: RFValue(35),
                                                marginBottom: 0,
                                                borderWidth: 0,
                                                backgroundColor: colors.green_1st,
                                                borderRadius: 5,
                                            }}
                                            onPress={() => this.props.navigation.navigate('Register')}
                                        >
                                            <Text style={styles.btnSign}>SIGN UP</Text>
                                        </Button>
                                </View>
                                <View style={{height:RFValue(35)}}>
                                        <Button
                                            block
                                            transparent
                                            style={{
                                                width:'100%',
                                                height: RFValue(35),
                                                marginBottom: 0,
                                                borderWidth: 1,
                                                borderColor: colors.white_1st,
                                                borderRadius: 5,
                                                marginTop:RFValue(10)
                                            }}
                                            onPress={() => this.props.navigation.navigate('Login')}
                                        >
                                            <Text style={styles.btnSign}>LOGIN</Text>
                                        </Button>
                                </View>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    content:{
        flex: 1, flexDirection: 'column',
        paddingHorizontal:RFValue(10),
    },
    titleGood:{
        fontSize:RFValue(15),
        color:colors.white_1st,
    },
    iconNav: {
        width:RFValue(20),
        height:RFValue(20),
        resizeMode:"stretch",
    },
    titleLogin: {
        fontSize:RFValue(30),
        color:colors.blue_2st,
        fontFamily: 'Montserrat-Regular',
    },
    signIn: {
        fontSize: size.small_04,
        color:colors.gray_1st,
        marginLeft:RFValue(10),
        fontFamily: 'Montserrat-Regular',
        fontWeight:'bold',
    },
    forgot:{
        fontSize:size.small_03,
        color:colors.gray_1st,
        textAlign:'right'
    },
    dont:{
        color:colors.gray_1st,
        fontSize:size.small_03,
        textAlign:'center'
    },
    create:{
        color:colors.black_1st,
        fontSize:size.small_03,
        fontWeight:'bold'
    },
    btnSign:{
        color:colors.white_1st,
        fontSize:RFValue(12),
        fontFamily: 'Montserrat-Regular',
    },
    icon:{
        justifyContent: 'center',
        marginLeft: RFValue(10),
        width:"10%",
    },
    loginButton: {
        marginBottom: 20,
    },
    flex:{
        flex:1,
        flexDirection:'row'
    },
    card:{
        marginLeft: RFValue(40),
        marginRight: RFValue(40),
        borderRadius: 50,
        marginTop:RFValue(18),
        backgroundColor:colors.white_1st,
        height:RFValue(35),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 5,
    },
    rowField: {
        flex: 1,
        marginLeft: 0,
        justifyContent:'center'
    },
    rowFieldPassword: {
        flex: 1,
        flexDirection:'row',
        width:'100%',
        marginLeft: 0,
    },
    input: {
        fontSize:RFValue(14),
        color:colors.gray,
        marginLeft:RFValue(5),
        marginRight: 15,
        fontFamily: 'Montserrat-Regular',
    },
    inputPassword: {
        fontSize:RFValue(14),
        color:colors.gray,
        marginLeft:RFValue(5),
        marginRight: 15,
        width:'80%',
        fontFamily: 'Montserrat-Regular',
    }
});

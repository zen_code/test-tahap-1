import React, { Component } from "react";
import {Platform, Image, AsyncStorage, StyleSheet} from "react-native";
import { Footer, FooterTab, Text, Button, Icon, View, Badge } from "native-base";
import {RFValue} from "react-native-responsive-fontsize";
import colors from "../styles/colors/."
import IconMC from "react-native-vector-icons/MaterialCommunityIcons";

class CustomFooter extends Component {
  constructor(props) {
    super(props);
    this.state={
      notif:0
    }
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      // this.loadNotif();
    });
    //this.loadData();

  }

  render() {

    return (
        <View>
          <Footer style={{backgroundColor:'gray', height:10}}>
            <FooterTab style={{ backgroundColor: colors.white_1st, height:60}}>
              <Button
                  vertical
                  onPress={() => this.props.navigation.navigate("MenuDrinks")}
                  style={{borderColor:colors.green_1st, borderBottomWidth: this.props.tab == "MenuDrinks" ? 2 : 0}}
              >
                <Text style={[{color: this.props.tab == "MenuDrinks" ? colors.green_1st : colors.gray_1st},styles.title]}>DRINKS</Text>
              </Button>
              <Button
                  vertical
                  onPress={() => this.props.navigation.navigate("MenuFood")}
                  style={{borderColor:colors.green_1st, borderBottomWidth: this.props.tab == "MenuFood" ? 2 : 0}}
              >
                <Text style={[{color: this.props.tab == "MenuFood" ? colors.green_1st : colors.gray_1st, fontSize:RFValue(10)},styles.title]}>FOOD</Text>
              </Button>
              <Button
                  vertical
                  onPress={() => this.props.navigation.navigate("MenuMerchandise")}
                  style={{borderColor:colors.green_1st, borderBottomWidth: this.props.tab == "MenuMerchandise" ? 2 : 0}}
              >
                <Text style={[{color: this.props.tab == "MenuMerchandise" ? colors.green_1st : colors.gray_1st, fontSize:RFValue(10)},styles.title]}>MERCHANDISE</Text>
              </Button>

            </FooterTab>
          </Footer>
        </View>
    );
  }
}

export default CustomFooter;

const styles = StyleSheet.create({
  icon: {
    width:RFValue(25),
    height:RFValue(25),
  },
  icon2: {
    width:RFValue(23),
    height:RFValue(23),
  },
  title:{
    fontSize:RFValue(12),
  }
})

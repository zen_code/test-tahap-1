import React, { Component } from "react";
import {
  Platform,
  Image,
  AsyncStorage,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import {
  Footer,
  FooterTab,
  Text,
  Button,
  Icon,
  View,
  Left,
  Body,
  Title,
  Right,
  Header} from "native-base";
import {RFValue} from "react-native-responsive-fontsize";
import IconMI from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityicons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from "../styles/colors";
import MyView from "../view/MyView";
const nav = require("../../component/assets/Nav_Drawer_Black.png");


class CustomHeader2 extends Component {
  constructor(props) {
    super(props);
    this.state={

    }
  }

  componentDidMount() {

  }

  render() {
    return (
        <View>
          <Header transparent style={styles.header}>
            <Left style={{ flex: 1, paddingLeft:RFValue(5)}}>
              <MyView hide={!this.props.left}>
                <TouchableOpacity
                    transparent
                    onPress={()=>this.props.navigation.navigate('Dashboard')}
                >
                  <Image style={styles.iconNav} source={nav}/>
                </TouchableOpacity>
              </MyView>
            </Left>
            <Body style={{ flex:3, alignItems:'center'}}>
              <Text style={styles.title}>{this.props.title}</Text>
            </Body>
            <Right style={{ flex: 1}}>
              <MyView hide={!this.props.right}>
                <TouchableOpacity
                    transparent
                    onPress={()=>this.props.navigation.goBack()}
                >
                </TouchableOpacity>
              </MyView>
            </Right>
          </Header>
        </View>
    );
  }
}

export default CustomHeader2;

const styles = StyleSheet.create({
  iconNav: {
    width:RFValue(20),
    height:RFValue(20),
    resizeMode:"stretch",
  },
  iconRight: {
    color:colors.black_1st,
    fontSize:RFValue(25),
    marginLeft:RFValue(-20)
  },
  title: {
    fontSize:RFValue(15),
    color:colors.black_1st,
  }
});


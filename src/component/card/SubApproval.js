import React, { Component } from "react";
import {View, Text, Image, TouchableNativeFeedback, StyleSheet} from "react-native";
import { Icon } from "native-base";
import colors from "../styles/colors/index";
import PropTypes from "prop-types";
import {RFValue} from "react-native-responsive-fontsize";

export default class SubApproval extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { loading, disabled, handleOnPress } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.cardLeft}>
                    <Text style={styles.title} numberOfLines={2}>{this.props.title}</Text>
                    <Text style={styles.dokter} numberOfLines={2}>{this.props.dokter}</Text>
                </View>
                <View style={{width:'3%'}}/>
                <View style={styles.cardRight}>
                    <Text style={styles.time} numberOfLines={1}>{this.props.time}</Text>
                    <Text style={styles.menit} numberOfLines={1}>Menit</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 0,
        marginRight: 0,
        marginBottom: RFValue(10),
        flexDirection:'row'
    },
    icon: {
        width: RFValue(40),
        height: RFValue(40),
    },
    cardLeft: {
        width: '77%',
        backgroundColor:colors.white_1st,
        borderRadius:RFValue(5),
        padding:RFValue(10),

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 1,
    },
    cardRight: {
        width: '20%',
        backgroundColor:colors.white_1st,
        borderRadius:RFValue(5),
        padding:RFValue(10),
        justifyContent:'center',
        alignItems:'center',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 1,
    },
    title: {
        fontSize:RFValue(12),
        color:colors.black_1st,
        fontFamily: 'Montserrat-Regular',
    },
    dokter: {
        fontSize:RFValue(13),
        color:colors.gray_1st,
        fontFamily: 'Montserrat-LightItalic',
    },
    time: {
        fontSize:RFValue(17),
        color:colors.black_1st,
        fontFamily: 'Montserrat-Regular',
    },
    menit: {
        fontSize:RFValue(12),
        color:colors.gray_1st,
        fontFamily: 'Montserrat-Regular',
    }
})


SubApproval.propTypes = {
    handleOnPress: PropTypes.func,
    disabled: PropTypes.bool
};



